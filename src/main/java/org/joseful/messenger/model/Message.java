package org.joseful.messenger.model;

import java.time.LocalDate;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Message {
    
    private long id;
    private String message;
    private LocalDate created;
    private String author;
    
    public Message() {}
    
    public Message(long id, String message, String author) {
        this.id = id;
        this.message = message;
        this.created = LocalDate.now();
        this.author = author;
    }
    
    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public LocalDate getCreated() {
        return created;
    }
    
    public void setDate(LocalDate date) {
        this.created = date;
    }
    
    public String getAuthor() {
        return author;
    }
    
    public void setAuthor(String author) {
        this.author = author;
    }   
}
